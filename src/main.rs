#![feature(box_syntax)]
extern crate rk_rs;
extern crate cgmath;
extern crate nalgebra;
use cgmath::Matrix2;
use cgmath::SquareMatrix;
mod newton;

use newton::newton;

const EXAMPLE: i64 = 5;

//IMPORTANT: don't forget to change start values
//Functions to change
fn f(values: &[f64]) -> f64 {
    if EXAMPLE == 1 {
        values[2] / values[1]
    }
    else if EXAMPLE == 2{
            2.0 * values[0] - values[1] - 2.0 * values[2].powf(2.0)  + 3.0 * values[2] + 1.0
    }
    else if EXAMPLE == 3{
        values[1]
    }
    else if EXAMPLE == 4{
        -2.0 * values[0] + values[1] + 100.
    }
    else{
        -values[0]
    }
}

fn g(values: &[f64]) -> f64 {
    if EXAMPLE == 1 {
        -values[2] / values[0]
    }
    else if EXAMPLE == 2{
            values[0] - 2.0*values[1] - values[2].powf(2.0) + 2.0*values[2] + 3.0
    }
    else if EXAMPLE == 3{
        -1000.*(values[1]*(values[0].powf(2.0) - 1.0) + values[0])
    }
    else if EXAMPLE == 4{
        10000.*values[0] - 10000.*values[1] + 50.0
    }
    else{
        -1000.0 * values[1]
    }
}

fn f_start_det_x(values: &[f64], step: f64) -> f64{
    if EXAMPLE == 1 {
        0.0
    }
    else if EXAMPLE == 2{
        2.0
    }
    else if EXAMPLE == 3{
        0.0
    }
    else if EXAMPLE == 4{
        -2.0
    }
    else{
        -1.0
    }
}

fn f_start_der_y(values: &[f64], step: f64) -> f64{
    if EXAMPLE == 1 {
        -values[2]/values[1].powf(2.0)
    }
    else if EXAMPLE == 2{
            -1.0
    }
    else if EXAMPLE == 3{
        1.0
    }
    else if EXAMPLE == 4{
        1.0
    }
    else{
        0.0
    }
}



fn g_start_der_x(values: &[f64], step: f64) -> f64{
    if EXAMPLE == 1 {
        values[2]/values[1].powf(2.)
    }
    else if EXAMPLE == 2{
            1.0
    }
    else if EXAMPLE == 3{
        -1000.0*(2.0 * values[0] * values[1] + 1.0)
    }
    else if EXAMPLE == 4{
        10000.0
    }
    else{
        0.0
    }
}

fn g_start_der_y(values: &[f64], step: f64) -> f64{
    if EXAMPLE == 1 {
        0.0
    }
    else if EXAMPLE == 2{
        -2.0
    }
    else if EXAMPLE == 3{
        -1000.0*(values[0].powf(2.0) - 1.0)
    }
    else if EXAMPLE == 4{
        -10000.0
    }
    else{
        -1000.0
    }
}

////////////////////////////
fn f_der_x(values: &[f64], step: f64) -> f64{
    1.0 - 12./25.*step*f_start_det_x(values, step)
}

fn f_der_y(values: &[f64], step: f64) -> f64{
    12./25. * step * g_start_der_x(values, step)
}

fn g_der_x(values: &[f64], step: f64) -> f64{
    12./25. * step * f_start_der_y(values, step)
}

fn g_der_y(values: &[f64], step: f64) -> f64{
    1.0 - 12./25.*step*g_start_der_y(values, step)
}




fn main() {
    let len = 1.;
    let eps = 0.0001;
    let mut step = 0.005/3.0;
    let mut n = 6;
    let k_coefficients = vec![vec![1., 1.], vec![1. / 2., 1. / 2.]];
    let next_iter_coef = vec![0., 1.];
    let mut values = vec![1., 1., 0.];
    if EXAMPLE == 1 {
        values = vec![1., 1., 0.];
    }
    else if EXAMPLE == 2{
        values = vec![0., 1., 0.];
    }
    else if EXAMPLE == 3{
        values = vec![2.0, 0.0, 0.0];
    }
    else if EXAMPLE == 4{
        values = vec![0.0, 0.0, 0.0];
    }
    else{
        values == vec![1.0, 1.0, 0.0];
    }
    let functions: Vec<Box<Fn(&[f64]) -> f64>> = vec![box f, box g];
    let functions = rk_rs::Functions::new(&functions);
    let rk =
        &mut rk_rs::RkInitial::new(&functions, &k_coefficients, &next_iter_coef, step, &values);
    loop {
        let first = rk.nth(n).unwrap();

        step /= 2.;
        n *= 2;

        rk.restore_initial_values();
        rk.set_step(step);

        let second = rk.nth(n).unwrap();
        rk.restore_initial_values();

        if stop_condition(&first, &second, eps) {
            println!("reached, n = {}, step = {}", n, step);
            break;
        }
    }
    let step_to_print = 0.1;
    let mut next_input = Vec::new();
    next_input.push(values.clone());
    let mut current_print_step = step_to_print;
    let mut values: Vec<Vec<f64>> = rk.take(4).collect();
    let mut t = 4. * step;
    let n = (len / step) as i64;
    let mut temp_step = 0.0;
    for i in 4..n/2/10 {
        t += step;
        let next_value = newton(step, &values, t, eps);
        shift_values(&mut values, next_value);
//        if (current_print_step - t).abs() < step / 2. {
            println!("t: {}, (y, x): {:?}", t, values[3]);
//            current_print_step += step_to_print;
//        }
        if i == n/2/3/10 {
            temp_step = t;
            next_input.push(values[3].clone());
        }
        else if i == n/3/10{
            next_input.push(values[3].clone())
        }
        if EXAMPLE == 5 || EXAMPLE == 1 || EXAMPLE == 2{
            if divergence_condition(&values[3], t, eps) {
                panic!()
            }
        }

    }
    next_input.push(values[3].clone());
    values = next_input;
//    step = 1.0/(n as f64/2./10./3.);
    step = temp_step;
    loop {
        t += step;
        let next_value = newton(step, &values, t, eps);
        shift_values(&mut values, next_value);
        if (current_print_step - t).abs() < step / 2. {
            println!("t: {}, (y, x): {:?}", t, values[3]);
            current_print_step += step_to_print;
        }
        if t  > 1. {
            return;
        }
    }
}

fn shift_values(values: &mut Vec<Vec<f64>>, next_value: Vec<f64>) {
    for i in 0..values.len() - 1 {
        values.swap(i, i + 1);
    }
    *values.last_mut().unwrap() = next_value;
}

fn stop_condition(first: &[f64], second: &[f64], eps: f64) -> bool {
    let mut accumulator = 0.;
    for i in 0..first.len() - 1 {
        accumulator += (first[i] - second[i]).abs();
    }
    accumulator < eps
}

fn divergence_condition(values: &[f64], t: f64, eps: f64) -> bool {
    (values[0] - x_precise(t)).abs() + (values[1] - y_precise(t)).abs() > eps
}

fn x_precise(t: f64) -> f64 {
    use std::f64::consts::E;
    if EXAMPLE == 1 {
        E.powf(t * t / 2.)
    }
    else if EXAMPLE == 2 {
        t*t
    }
    else if EXAMPLE == 5{
        E.powf(-t)
    }
    else{
        panic!()
    }
}

fn y_precise(t: f64) -> f64 {
    use std::f64::consts::E;
    if EXAMPLE == 1 {
        E.powf(-(t * t) / 2.)
    }
    else if EXAMPLE == 2 {
        t + 1.0
    }
    else if EXAMPLE == 5 {
        E.powf(-1000. * t)
    }
    else{
        panic!()
    }
}
