use super::{f, g, f_der_x, f_der_y, g_der_x, g_der_y};
use nalgebra::{Matrix2, Matrix2x1};
use nalgebra::linalg::Cholesky;
pub fn newton(step: f64, values: &[Vec<f64>], t: f64, eps: f64) -> Vec<f64> {
    let mut x_local = values[3][0];
    let mut y_local = values[3][1];
    let multi = 12. / 25. * step;
    let const_values = calculate_const(values);
    for _ in 0..10 {
        let x_last = x_local;
        let y_last = y_local;
        let v = delta_x(step, &const_values, x_local, y_local, t,&f,&g);
        x_local = x_last + v[0];
        y_local = y_last + v[1];
        if ((x_last - x_local).abs() + (y_last - y_local).abs()) < eps {
            return vec![x_local, y_local];
        }
    }
    panic!()
}


fn delta_x(step: f64, const_values: &[f64], x: f64, y: f64, t: f64, f: &Fn(&[f64]) -> f64, g: &Fn(&[f64]) -> f64) -> Vec<f64>{
    let values = &[x,y,t];
    let a = Matrix2::new(f_der_x(values, step), f_der_y(values, step), g_der_x(values, step), g_der_y(values, step));
    let a = Cholesky::new(a).unwrap();
    let b = Matrix2x1::new(-f_local(const_values[0],x,y,t, step, f), -g_local(const_values[1],x,y,t, step,g));
    let x = a.solve(&b);
    let mut one = 0.0;
    let mut two = 0.0;
    unsafe {
        one = (*x.get_unchecked(0,0)).clone();
        two = (*x.get_unchecked(1,0)).clone();
    }
    return vec![one, two];
}


fn calculate_const(values: &[Vec<f64>]) -> Vec<f64> {
    vec![
        rest(values[0][0], values[1][0], values[2][0], values[3][0]),
        rest(values[0][1], values[1][1], values[2][1], values[3][1]),
    ]
}

fn rest(var_0: f64, var_1: f64, var_2: f64, var_3: f64) -> f64 {
    -(48. / 25. * var_3 - 36. / 25. * var_2 + 16. / 25. * var_1 - 3. / 25. * var_0)
}

fn f_local(const_value: f64, x_last: f64, y_last: f64, t: f64, step: f64, f: &Fn(&[f64]) -> f64) -> f64{
    x_last + const_value - 12./25. * step * f(&[x_last, y_last, t])
}

fn g_local(const_value: f64, x_last: f64, y_last: f64, t: f64, step: f64, g: &Fn(&[f64]) -> f64) -> f64{
    y_last + const_value - 12./25. * step * g(&[x_last, y_last, t])
}